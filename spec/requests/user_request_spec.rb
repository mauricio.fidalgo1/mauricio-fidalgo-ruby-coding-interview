require 'rails_helper'

RSpec.describe "Users", type: :request do

  RSpec.shared_context 'with multiple companies' do
    let!(:company_1) { create(:company) }
    let!(:company_2) { create(:company) }

    before do
      5.times do
        create(:user, company: company_1)
      end
      5.times do
        create(:user, company: company_2)
      end
    end
  end

  RSpec.shared_context 'with pre-defined names' do
    let!(:users_with_ma_in_username) {
      [
        create(:user, username: 'mathew'),
        create(:user, username: 'max'),
        create(:user, username: 'mauricio')
      ]
    }
    
    let!(:users_that_dont_belong) {
      [
        create(:user, username: 'example'),
        create(:user, username: 'notbelong')
      ]
    }
  end

  describe "#index" do
    let(:result) { JSON.parse(response.body) }

    context 'when fetching users by company' do
      include_context 'with multiple companies'

      it 'returns only the users for the specified company' do
        get company_users_path(company_1)
        
        expect(result.size).to eq(company_1.users.size)
        expect(result.pluck('id') ).to eq(company_1.users.ids)
      end
    end

    context 'when fetching all users' do
      include_context 'with pre-defined names'

      it 'returns all the users' do
        get users_path(username: 'ma')

        result_ids = result.pluck('id')
        expect(result.size).to eq(users_with_ma_in_username.size)
        expect(result_ids).to eq(users_with_ma_in_username.pluck(:id))
        expect(result_ids).not_to include(users_that_dont_belong.pluck(:id)) 
      end
    end
  end
end
